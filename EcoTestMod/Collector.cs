﻿using System.Threading;

namespace Eco.Plugins.EcoTestMod
{
    public class Collector
    {
        private int POLL_DELAY;

        public void initialize(int delay = 5000)
        {
            POLL_DELAY = delay;
        }

        public void collect()
        {
            while (true)
            {
                Logger.Debug("collecting");

                Thread.Sleep(POLL_DELAY);
            }
        }
    }
}
