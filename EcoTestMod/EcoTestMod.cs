﻿using System;
using System.ComponentModel;
using System.Threading;
using Eco.Core.Plugins;
using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;

namespace Eco.Plugins.EcoTestMod
{
    public class EcoTestMod : IModKitPlugin, IInitializablePlugin, IConfigurablePlugin
    {
        private PluginConfig<ModConfig> configOptions;
        protected Collector collector;
        private string status = "initializing";

        public IPluginConfig PluginConfig
        {
            get { return configOptions; }
        }

        public object GetEditObject()
        {
            return configOptions.Config;
        }

        public string GetStatus()
        {
            return status;
        }

        public void Initialize(TimedTask timer)
        {
            Logger.DebugVerbose("initializing");
            collector.initialize();
            try
            {
                new Thread(() => { collector.collect(); })
                {
                    Name = "Collector"
                }.Start();
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        public void OnEditObjectChanged(object o, string param)
        {
            Logger.DebugVerbose("Saving Config");
            configOptions.Save();
        }

        public class ModConfig
        {
            [Description("The name of the Eco server, overriding the name configured within Eco."), Category("Server Details")]
            public string api_url { get; set; }

            [Description("The token provided by the Discord API to allow access to the bot"), Category("Bot Configuration")]
            public int poll_interval { get; set; }
        }
    }
}
